
console.log('program starting')
const router = require('./router');
const PORT = 3000;
const HOST = "127.0.0.1";


router.listen(
    PORT, 
    HOST,
    () => console.log(`Listening to http://${HOST}:${PORT}`)
    
);

console.log('program ending')